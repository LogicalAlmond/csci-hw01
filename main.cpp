#include <iostream>
#include <string>

float getPercentage(float a, float b) {
    return (a / b) * 100.00;
}

int main() {

    srand (time(NULL));

    const char sequences[3] = { 'A', 'T', 'C' };

    int organismSelection; 
    int sequenceLength;
    float countA = 0;
    float countT = 0;
    float countC = 0;
    float countTotal = 0;
    std::string sequenceString;
 
    std::cout << "Select an organism: \n" <<
                 "1) Chicken\n" <<
                 "2) E. Coli\n" <<
                 "3) Grasshopper\n" <<
                 "4) Human\n" <<
                 "5) Maize\n" <<
                 "6) Octopus\n" <<
                 "7) Rat\n" <<
                 "8) Sea Urchin\n" <<
                 "9) Wheat\n" <<
                 "10) Yeast\n" <<
                 "\n";
    
    // loop:
    while (true){
        std::cout << "Selection: ";
        std::cin >> organismSelection;
        
        switch (organismSelection) {
        case 1:
            std::cout << "Chicken seleted\n";
            break;
        case 2:
            std::cout << "E. Coli selected\n";
            break;
        case 3:
            std::cout << "Grasshopper selected\n";
            break;
        case 4:
            std::cout << "Human selected\n";
            break;
        case 5:
            std::cout << "Maize selected\n";
            break;
        case 6:
            std::cout << "Octopus selected\n";
            break;
        case 7:
            std::cout << "Rat selected\n";
            break;
        case 8:
            std::cout << "Sea Urchin selected\n";
            break;
        case 9:
            std::cout << "Wheat selected\n";
            break;
        case 10:
            std::cout << "Yeast selected\n";
            break;
        default:
            std::cout << "Not a valid option.\n";
            continue;
        }
        break;
    }

    std::cout << "Sequence length: ";
    std::cin >> sequenceLength;
    std::cout << "\n";

    for (int i = 0; i < sequenceLength; i++) {
        char randomLetter = rand() % 3;
        sequenceString.push_back(sequences[randomLetter]);
    }

    for (char c : sequenceString) {
        countTotal ++;
        if (c == 'A') {
            countA ++;
        } else if (c == 'T') {
            countT ++;
        } else if (c == 'C') {
            countC ++;
        }
    }

    std::cout << "Sequence: " << sequenceString << "\n\n";

    std::cout << "Results: \n" << 
                 getPercentage(countA, countTotal) << "\% A\n" <<
                 getPercentage(countT, countTotal) << "\% T\n" <<
                 getPercentage(countC, countTotal) << "\% C\n";
    
    return 0;
}